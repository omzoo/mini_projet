package controle;

public final class Cesar {

	private static char chiffrement(char c, int shift) {
		int debut;
		if (('a' <= c) && (c <= 'z'))
			debut = 'a';
		else if (('A' <= c) && (c <= 'Z'))
			debut = 'A';
		else
			return c;
		return (char) ((c - debut + (shift < 0 ? shift + 26 : shift)) % 26 + debut);
	}

	public static String chiffrement(String str, int shift) {
		String res = "";
		for (char c : str.toCharArray())
			res += chiffrement(c, shift);
		return res;
	}


}
