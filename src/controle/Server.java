package controle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;

public class Server extends Thread {
	BufferedReader in;
	PrintWriter out;
	int port =56789;
	private ArrayList<Socket> clients = new ArrayList<>(); 
	int idClient;

	public static void main(String[] args) {
		new Server().start();
	}

	@Override
	public void run() {  // On redifinit la méthode
		try {
			ServerSocket ss = new ServerSocket(12345);
			while(true) {				
				Socket socket = ss.accept();
				//System.out.println("Un client vient de se connecter");
				clients.add(socket);
				++idClient;
				new Conversation(socket, idClient).start();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} 

	}


	class Conversation extends Thread { 

		private Socket socket;
		private int idClient;

		public Conversation(Socket socket, int idClient) {
			this.socket = socket;
			this.idClient = idClient;
			System.out.println("Bienvenue vous etes le client " + idClient);

			String ip = socket.getRemoteSocketAddress().toString();
			System.out.println("Connexion avec l'ip : " + ip);
		}

		public void run(){
			try {
				
				BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				PrintWriter out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()),true);
				//demander chaine tcp ou udp
				long IDC;
				Random ran = new Random();
				String ligne;
				while(true) {
					
					IDC = ran.nextLong();
					ligne = in.readLine().toUpperCase();
					while(IDC < 0) {
						IDC = ran.nextLong();
					}
					String reponse = "";
					switch(ligne) {
					case "UDP":
						reponse += IDC + ":UDP:56789";
						out.println(reponse);
						break;

					case "TCP":
						reponse += IDC + ":TCP:56789";
						out.println(reponse);
						break;

					default:
						out.println("Au revoir");
						socket.close();

					}

				}

			}catch (IOException e) {
			}
		}
	}



	public void broadCast(String message) {
		for(Socket client : clients) {
			try {
				PrintWriter printWriter = new PrintWriter(client.getOutputStream(), true);
				printWriter.println(message); 
			} catch (IOException e) {

				e.printStackTrace();
			}
		}
	}
}









